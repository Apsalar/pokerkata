﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Poker;
using System.Linq;

namespace PokerTest
{
	[TestClass]
	public class PokerScoreTest
	{
		HandScorer _hs;
		Card[] _cards;

		[TestInitialize]
		public void SetUp()
		{
			_hs = new HandScorer();
		}

		[TestMethod]
		public void HighCardTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "Q" },
										new Card { Suit = CardSuit.Diamonds, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "J" },
										new Card { Suit = CardSuit.Spades, Value = "A" }			
			};
			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.HighCard);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "A"));
		}

		[TestMethod]
		public void PairTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "Q" },
										new Card { Suit = CardSuit.Diamonds, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" },
										new Card { Suit = CardSuit.Spades, Value = "A" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.Pair);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "Q"));
		}

		[TestMethod]
		public void TwoPairsTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "Q" },
										new Card { Suit = CardSuit.Diamonds, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" },
										new Card { Suit = CardSuit.Spades, Value = "T" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.TwoPairs);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "Q"));
		}

		[TestMethod]
		public void ThreeOfAKindTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "A" },
										new Card { Suit = CardSuit.Diamonds, Value = "A" },
										new Card { Suit = CardSuit.Hearts, Value = "A" },
										new Card { Suit = CardSuit.Spades, Value = "2" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.ThreeOfAKind);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "A"));
		}

		[TestMethod]
		public void StraightTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "6" },
										new Card { Suit = CardSuit.Diamonds, Value = "4" },
										new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Spades, Value = "3" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.Straight);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "6"));
		}

		[TestMethod]
		public void StraightFromAce()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Clubs, Value = "A" },
										new Card { Suit = CardSuit.Diamonds, Value = "4" },
										new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Spades, Value = "3" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.Straight);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "5"));
		}

		[TestMethod]
		public void StraightToAce()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Diamonds, Value = "T" },
										new Card { Suit = CardSuit.Clubs, Value = "A" },
										new Card { Suit = CardSuit.Diamonds, Value = "J" },
										new Card { Suit = CardSuit.Hearts, Value = "K" },
										new Card { Suit = CardSuit.Spades, Value = "Q" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.Straight);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "A"));
		}

		[TestMethod]
		public void FlushTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "4" },
										new Card { Suit = CardSuit.Hearts, Value = "J" },
										new Card { Suit = CardSuit.Hearts, Value = "K" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.Flush);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "K"));
		}

		[TestMethod]
		public void FullHouseTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "T" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" },
										new Card { Suit = CardSuit.Hearts, Value = "Q" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.FullHouse);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "Q"));
		}

		[TestMethod]
		public void FourOfaKindTest()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Hearts, Value = "J" },
										new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Hearts, Value = "2" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.FourOfAKind);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "2"));
		}

		[TestMethod]
		public void StraightFlushFromAce()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "A" },
										new Card { Suit = CardSuit.Hearts, Value = "3" },
										new Card { Suit = CardSuit.Hearts, Value = "2" },
										new Card { Suit = CardSuit.Hearts, Value = "5" },
										new Card { Suit = CardSuit.Hearts, Value = "4" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.StraightFlush);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "5"));
		}

		[TestMethod]
		public void StraightFlushToAce()
		{
			_cards = new Card[5] { new Card { Suit = CardSuit.Diamonds, Value = "T" },
										new Card { Suit = CardSuit.Diamonds, Value = "A" },
										new Card { Suit = CardSuit.Diamonds, Value = "J" },
										new Card { Suit = CardSuit.Diamonds, Value = "K" },
										new Card { Suit = CardSuit.Diamonds, Value = "Q" }};

			Score score = _hs.Score(_cards);

			Assert.IsTrue(score.Rank == Rank.StraightFlush);
			Assert.IsNotNull(score.Cards.SingleOrDefault(c => c.Value == "A"));
		}
	}
}
