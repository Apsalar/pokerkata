﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
	public class HandScorer
	{
		private Dictionary<string, int> CardValues = new Dictionary<string, int>()
		{
			{ "2", 2 },
			{ "3", 3 },
			{ "4", 4 },
			{ "5", 5 },
			{ "6", 6 },
			{ "7", 7 },
			{ "8", 8 },
			{ "9", 9 },
			{ "T", 10 },
			{ "J", 11 },
			{ "Q", 12 },
			{ "K", 13 },
			{ "A", 14 },
		};

		private Dictionary<string, int> CardValuesFromAce = new Dictionary<string, int>()
		{
			{ "A", 1 },
			{ "2", 2 },
			{ "3", 3 },
			{ "4", 4 },
			{ "5", 5 },
			{ "6", 6 },
			{ "7", 7 },
			{ "8", 8 },
			{ "9", 9 },
			{ "T", 10 },
			{ "J", 11 },
			{ "Q", 12 },
			{ "K", 13 }			
		};

		public Score Score(Card[] hand)
		{
			Score s;

			// Straight Flush
			bool isFlush = hand.All(c => c.Suit == hand[0].Suit);

			if (isFlush)
			{
				s = CheckStraight(hand, CardValues) ?? CheckStraight(hand, CardValuesFromAce);

				if (s != null)
				{
					s.Rank = Rank.StraightFlush;
					return s;
				}
			}

			// Four of a Kind

			var foruOfAKind = hand.GroupBy(c => c.Value).Where(g => g.Count() == 4).FirstOrDefault();

			if (foruOfAKind != null)
			{
				s = new Score() { Rank = Rank.FourOfAKind };
				s.Cards.Add(foruOfAKind.First());

				return s;
			}

			// Full House
			var fullHouse =
					new
					{
						Pair = hand.GroupBy(g => g.Value).Where(g => g.Count() == 2).FirstOrDefault(),
						ThreeOfAKind = hand.GroupBy(g => g.Value).Where(g => g.Count() == 3).FirstOrDefault()
					};

			if (fullHouse.Pair != null && fullHouse.ThreeOfAKind != null)
			{
				s = new Score() { Rank = Rank.FullHouse };
				s.Cards.Add(fullHouse.ThreeOfAKind.First());

				return s;
			}

			// Flush
			isFlush = hand.All(c => c.Suit == hand[0].Suit);

			if (isFlush)
			{
				Card maxFlushCard = hand.First(i => CardValues[i.Value] == hand.Max(c => CardValues[c.Value]));

				s = new Score() { Rank = Rank.Flush };
				s.Cards.Add(maxFlushCard);

				return s;
			}

			// Straight

			s = CheckStraight(hand, CardValues) ?? CheckStraight(hand, CardValuesFromAce);

			if (s != null)
			{
				return s;
			}

			// Three Of A Kind

			var threeOfAKind = hand.GroupBy(c => c.Value).Where(g => g.Count() == 3).Select(g => g.First()).FirstOrDefault();

			if (threeOfAKind != null)
			{
				s = new Score() { Rank = Rank.ThreeOfAKind };
				s.Cards.Add(threeOfAKind);

				return s;
			}

			// Two Pairs

			var twoPairsCard = hand.GroupBy(c => c.Value).Where(g => g.Count() == 2).Select(g => g.First());

			if (twoPairsCard.Count() == 2)
			{
				s = new Score() { Rank = Rank.TwoPairs };
				s.Cards.AddRange(twoPairsCard);

				return s;
			}

			// Pair

			var pairCard = hand.GroupBy(c => c.Value).Where(g => g.Count() == 2).Select(g => g.First()).FirstOrDefault();

			if (pairCard != null)
			{
				s = new Score() { Rank = Rank.Pair };
				s.Cards.Add(pairCard);

				return s;
			}

			// High Card

			Card maxCard = hand.First(i => CardValues[i.Value] == hand.Max(c => CardValues[c.Value]));

			s = new Score() { Rank = Rank.HighCard };
			s.Cards.Add(maxCard);

			return s;
		}

		private Poker.Score CheckStraight(Card[] hand, Dictionary<string, int> dict)
		{
			Score s = new Score();
			var sortedstraightHand = hand.OrderBy(c => dict[c.Value]).Select(c => dict[c.Value]);

			var straight = Enumerable.Range(sortedstraightHand.First(), 5);

			if (!straight.Except(sortedstraightHand).Any())
			{
				s = new Score() { Rank = Rank.Straight };
				s.Cards.Add(hand.OrderBy(c => dict[c.Value]).Last());
				return s;
			}
			return null;
		}
	}
}
