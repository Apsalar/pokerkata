﻿using Poker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
	public enum CardSuit
	{
		Clubs,
		Diamonds,
		Hearts,
		Spades
	}

	public class Card
	{
		public CardSuit Suit { get; set; }
		public string Value { get; set; }

		public override string ToString()
		{
			return string.Format("{0} {1}", Value, Suit);
		}
	}
}
