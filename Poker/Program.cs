﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
	class Program
	{
		static void Main(string[] args)
		{

			var _hs = new HandScorer();
			var _cards = new Card[5] { new Card { Suit = CardSuit.Hearts, Value = "3" },
										new Card { Suit = CardSuit.Clubs, Value = "5" },
										new Card { Suit = CardSuit.Clubs, Value = "4" },
										new Card { Suit = CardSuit.Spades, Value = "2" },
										new Card { Suit = CardSuit.Clubs, Value = "T" }};

			Score score = _hs.Score(_cards);

			Console.WriteLine("{0}, Cards = {1}", score.Rank.ToString(), string.Join(",", score.Cards));

			Console.ReadLine();
		}
	}
}
