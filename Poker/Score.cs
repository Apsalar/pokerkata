﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
	public enum Rank
	{
		HighCard,
		Pair,
		TwoPairs,
		ThreeOfAKind,
		Straight,
		Flush,
		FullHouse,
		FourOfAKind,
		StraightFlush
	}

	public class Score
	{
		public Rank Rank { get; set; }
		public List<Card> Cards { get; set; }

		public Score()
		{
			Cards = new List<Card>(); ;
		}
	}
}
